import csv
from collections import defaultdict
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", type=str, nargs="?")
args = vars(parser.parse_args())

file = args["file"]

with open(file ,"r") as f:
    d = []
    reader = csv.reader(f)
    data = list(reader)

    for row in data[1:]:
        d.append(float(row[-2]))   

print("Median is {}".format(sorted(d)[len(d) // 2]))
