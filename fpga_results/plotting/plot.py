#################################################
# Creates two subplots to improve readability
# Data is splitted according a threshold:
# - upper part is for benchmark running in less than Thr seconds (1 by default)
# - lower part is for the other




import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import argparse


THR = 1 			# threshold for splitting the data in two

CUT_ABOVE_THRESHOLD = True	# cut the values above a given threhold 
UPPER_THRESHOLD = 200

########################
# read and adjust data


parser = argparse.ArgumentParser()
parser.add_argument("-i", "--intel_fpga_csv", type=str, nargs="?", default="../intel_fpga/results_summary.csv")
parser.add_argument("-x", "--xilinx_csv", type=str, nargs="?", default="../xilinx/results_summary.csv")

args = vars(parser.parse_args())
intel_file = args["intel_fpga_csv"]
xilinx_file = args["xilinx_csv"]



intel = pd.read_csv(intel_file)
intel.columns = ['Benchmark', 'Time']
xilinx = pd.read_csv(xilinx_file)
xilinx.columns = ['Benchmark', 'Time']

# add hue column
intel['Platform'] = "DaCe on Intel FPGA"
xilinx['Platform'] = "DaCe on Xilinx FPGA"

# Use seconds instead of mseconds
intel['Time'] = intel['Time'] / 1000.0
xilinx['Time'] = xilinx['Time'] / 1000.0

renamings = {
	"Adi":"adi",
	"Atax":"atax",
	"Cholesky":"cholesky",
	"Jacobi 1D":"jacobi1d",
	"Jacobi 2D":"jacobi2d",
	"Heat 3d":"heat3d",
	"Gramschmidt":"gramschm",
	"Fdtd-2D":"fdtd_2d",
	"seidel 2D":"seidel",
	"Gemm":"gemm",
	"Correlation":"correlat",
	"Floyd-Warshall":"floydwar",
	"Doitgen":"doitgen",
	"Bicg":"bicg",
	"Durbin":"durbin",
	"Deriche":"deriche",
	"Covariance":"covarian",
	"Gemver":"gemver",
	"Gesummv":"gesummv"
}
intel = intel.replace(renamings)
xilinx = xilinx.replace(renamings)




# Split data in two. Splitting is done by looking at 
# Intel data, assuming intel-xilinx have the same index

# TODO: actually we should look at the slowest of the two (not a problem currently)


faster_benchmarks = pd.DataFrame([])
slower_benchmarks = pd.DataFrame([])

for index, value in intel.iterrows():
	# check that we are looking at the same data
	assert value.Benchmark == xilinx.loc[index].Benchmark
	if value.Time <= THR:	
		faster_benchmarks = faster_benchmarks.append(value, ignore_index=True)
		faster_benchmarks = faster_benchmarks.append(xilinx.loc[index], ignore_index=True)
	else:
		slower_benchmarks = slower_benchmarks.append(value, ignore_index=True)
		slower_benchmarks = slower_benchmarks.append(xilinx.loc[index], ignore_index=True)


# For the slower we cut the values to an upper limit, displaying the time when above this threshold

if CUT_ABOVE_THRESHOLD:
	cut_values = dict()

	for index, value in slower_benchmarks.iterrows():
		if value.Time > UPPER_THRESHOLD:
			cut_values[index] = value.Time
			slower_benchmarks.at[index, 'Time'] = UPPER_THRESHOLD

######################
# Plot



sns.set_style("whitegrid")


# subplots
fig, axes = plt.subplots(2, 1, figsize=(16, 10))


fast = sns.barplot(ax=axes[0], x='Benchmark', y='Time', hue='Platform', data=faster_benchmarks, palette=["#F55B4E", "#69A8F5"])
slow = sns.barplot(ax=axes[1], x='Benchmark', y='Time', hue='Platform', data=slower_benchmarks, palette=["#F55B4E", "#69A8F5"])


# Labels
fast.set_ylabel("Time [s]", fontsize = 20)
fast.set_xlabel("")

slow.set_ylabel("Time [s]", fontsize = 20)
slow.set_xlabel("")

#  Remove legend
# fast.get_legend().remove()
slow.get_legend().remove()


# Set limits and font sizes

# Upper part
plt.sca(axes[0])
fast.set_ylim(0,THR)
plt.yticks(fontsize = 16)
plt.xticks(rotation=90, fontsize=16)


# Lower part

plt.sca(axes[1])


if CUT_ABOVE_THRESHOLD:

	slow.set_ylim(0, UPPER_THRESHOLD + 30)

	# # Add line indicating the upper cut value
	slow.axhline(UPPER_THRESHOLD, ls='--', c='r')

	# Plot time value for unreadable stuff

	# Not sure how to this properly, since patches are not aligned with panda dataframe
	# So for the moment being is "manual"

	for (index, value), rect in zip(slower_benchmarks.iterrows(), slow.patches):
	    if index in cut_values:
	    	slow.text(index/2 - 0.27,
	               value.Time + 5,
	               '{0:.0f}'.format(cut_values[index]),
	               color='black', #if value.Platform == 'Intel FPGA' else 'C1',
	               ha="center",
	               rotation=90,
	               fontsize=14)



plt.yticks(fontsize = 18)
plt.xticks(rotation=90, fontsize=18)


plt.setp(fast.get_legend().get_texts(), fontsize='18') # for legend text
plt.setp(fast.get_legend().get_title(), fontsize='16') # for legend title

# add legend somewhere else
# handles, labels = fast.get_legend_handles_labels()
# fig.legend(handles, labels, loc='upper right', ncol=2, bbox_to_anchor=(.75, 0.98))


# plt.xticks(rotation=90, fontsize=14)
# plt.yticks(fontsize = 14)
# plt.yticks(range(0, upper_limit + 1, 25), fontsize=14)
plt.tight_layout()

#######################
# Set size and save

fig = plt.gcf()
fig.set_size_inches(16, 10)
plt.savefig('fpga_results.pdf', bbox_inches='tight')
plt.show()





####################
# Old stuff



# # We will show a line at the upper limit: all the times above are cut at that value, and later annotated
# cut_values = dict()

# for index, value in res.iterrows():
#     if value.Time > upper_limit:
#         cut_values[index] = value.Time
#         res.at[index, 'Time'] = upper_limit


# g.set_ylim(0, upper_limit + 30)

# # Add line indicating the upper cut value
# g.axhline(upper_limit, ls='--', c='r')

# # Plot time value for unreadable stuff

# for (index, value), rect in zip(res.iterrows(), g.patches):
#     if value.Time < lower_threshold:
#         g.text(rect.get_x() + rect.get_width() / 2,
#                value.Time + 5,
#                value.Time,
#                color='C0' if value.Platform == 'Intel FPGA' else 'C1',
#                ha="center",
#                rotation=90,
#                fontsize=12)

#     if index in cut_values:
#         g.text(rect.get_x() + rect.get_width() / 2,
#                value.Time + 5,
#                '{0:.0f}'.format(cut_values[index]),
#                color='C0' if value.Platform == 'Intel FPGA' else 'C1',
#                ha="center",
#                rotation=90,
#                fontsize=12)
