import pandas as pd
import numpy as np
import seaborn as sns 
import matplotlib.pyplot as plt

sns.set(rc={'figure.figsize':(8,4)})
sns.set_style("whitegrid")

data = pd.read_csv("NumPy Benchmarks - AOT Compilation.csv")
gpu_data = pd.read_csv("NumPy Benchmarks - GPU Compilation.csv")
fpga_data = pd.read_csv("intel_fpga_harware_synthesis_time.csv")
xilinx_data = pd.read_csv("xilinx_compilation_times.csv")
gcc_data = data.iloc[:, [0,4]]
data = data.iloc[:,[0,-1]]
nvcc_data = gpu_data.iloc[:,[0,5]]
gpu_data = gpu_data.iloc[:,[0,-1]]
fpga_data = fpga_data.iloc[:,[0,-1]]
data.columns = ['benchmark', 'time']
gcc_data.columns = ['benchmark', 'time']
gpu_data.columns = ['benchmark', 'time']
nvcc_data.columns = ['benchmark', 'time']
fpga_data.columns = ['benchmark', 'time']

# fig, axs = plt.subplots(nrows=1, ncols=2, sharex=False, sharey=False)

# df = pd.concat(axis=0, ignore_index=True, objs=[
#     pd.DataFrame.from_dict({'value': data['time'], 'Framework': 'DaCe CPU'}),
#     pd.DataFrame.from_dict({'value': gcc_data['time'] / 1000, 'Framework': 'GCC'})
# ])
# sns.histplot(data=df, x="value", hue='Framework', stat="count", binwidth=2, palette=["#F55B4E", "#EBD224"], ax=axs[0])
# axs[0].set(xlabel='Compilation Time [s]', ylabel='Number of Benchmarks')

# df = pd.concat(axis=0, ignore_index=True, objs=[
#     pd.DataFrame.from_dict({'value': gpu_data['time'], 'Framework': 'DaCe GPU'}),
#     pd.DataFrame.from_dict({'value': nvcc_data['time'] / 1000, 'Framework': 'NVCC'})
# ])
# sns.histplot(data=df, x="value", hue='Framework', stat="count", binwidth=2, palette=["#69A8F5", "#76b900"])
# axs[1].set(xlabel='Compilation Time [s]', ylabel='Number of Benchmarks')

fig, axs = plt.subplots(nrows=1, ncols=2, sharex=False, sharey=False)

df = pd.concat(axis=0, ignore_index=True, objs=[
    pd.DataFrame.from_dict({'value': data['time'], 'Architecture': 'CPU'}),
    pd.DataFrame.from_dict({'value': gpu_data['time'], 'Architecture': 'GPU'})
])
print(df)
sns.histplot(data=df, x="value", hue='Architecture', stat="count", binwidth=2, palette=["#F55B4E", "#69A8F5"], ax=axs[0])
axs[0].set(xlabel='Compilation Time [s]', ylabel='Number of Benchmarks')


df = pd.concat(axis=0, ignore_index=True, objs=[
    pd.DataFrame.from_dict({'value': fpga_data['time'], 'FPGA Vendor': 'Intel'}),
    pd.DataFrame.from_dict({'value': xilinx_data['time'] / 60, 'FPGA Vendor': 'Xilinx'}),
])
sns.histplot(data=df, x="value", hue='FPGA Vendor', stat="count", binwidth=30, palette=["#F55B4E", "#69A8F5"], ax=axs[1])
axs[1].set(xlabel='Compilation Time [min]', ylabel='Number of Benchmarks')


# ax = sns.histplot(data=data, x="time", stat="count", binwidth=2, color="#F55B4E")
# ax.set(xlabel='Compilation Time [s]', ylabel='Number of Benchmarks')
plt.tight_layout()
plt.savefig("comptimes.pdf", dpi=600)
plt.show()
