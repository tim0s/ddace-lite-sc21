# Setting up distributed benchmarks on CloudLab

The following assume an experiment where the nodes use Ubuntu 20.04.

1. It will probably be useful to make a disk-image after installing all the necessary software to load it on all the nodes. Therefore, we first make a new directory, because the home folder is not save in the disk-image (see https://docs.cloudlab.us/advanced-storage.html#%28part._local-storage-persist%29).
```bash
sudo mkdir /sc21 && cd /sc21
sudo chmod 775 .
```

2. We install mpich.
```bash
sudo apt update
sudo apt install mpich
```

3. We download anaconda/miniconda and set up  the environment (instructions for miniconda).
```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-py38_4.10.3-Linux-x86_64.sh
chmod +x Miniconda3-py38_4.10.3-Linux-x86_64.sh
./Miniconda3-py38_4.10.3-Linux-x86_64.sh  # Store in /sc21/miniconda3
source ~/.bashrc
conda create --name sc21_dace python=3.8
conda activate sc21_dace
```

4. We download and set up the artifact.
```bash
git clone https://spclgitlab.ethz.ch/tim0s/ddace-lite-sc21.git
cd ddace-lite-sc21/
git submodule update --init --recursive
cd benchmarks_distmem/
python -m pip install --editable .
python -m pip install --editable dace
python -m pip install pytest
```

5. We install MKL (using conda for simplicity; we can also install the ubuntu package)
```bash
conda install mkl mkl-include
export MKLROOT=/sc21/miniconda3/envs/sc21_dace
```

6. We install mpi4py.
```bash
python -m pip install mpi4py
```

7. There may be linking issues with MKL when running the benchmark script. We found the following environmental variables to help
```bash
export LD_LIBRARY_PATH=/lib/x86_64-linux-gnu:$MKLROOT/envs/sc21_dace
export LD_PRELOAD=$MKLROOT/lib/libmkl_core.so:$MKLROOT/lib/libmkl_gnu_thread.so:$MKLROOT/lib/libgomp.so:$MKLROOT/lib/libmkl_blacs_intelmpi_lp64.so:$MKLROOT/lib/libmkl_intel_lp64.so:/lib/x86_64-linux-gnu/libmpichcxx.s
```

8. We have prepared a DaCe environment file for MKL Scalapack that should work with the above setup. It is located in this artifact in `distributed/cloudlab/intel_mkl.py`. We copy this to `dace/libraries/pblas/environments`.
```bash
mv ../distributed/cloudlab/intel_mkl.py dace/dace/libraries/pblas/environments/intel_mkl.py
```

9. We run the benchmark script.
```bash
python sc21_bench.py
```
If we were running on, e.g., the C6320 nodes (2x Haswell 14-core CPUs), then we adjust the command as follows. To run on a single socket:
```bash
OMP_NUM_THREADS=14 DACE_compiler_use_cache=1 mpiexec -n 1 -bind-to socket python sc21_bench.py
```
To run on two sockets (whole node)
```bash
OMP_NUM_THREADS=14 DACE_compiler_use_cache=1 mpiexec -n 2 -bind-to socket python sc21_bench.py
```

