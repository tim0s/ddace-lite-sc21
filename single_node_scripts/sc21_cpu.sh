#!/bin/bash

export OMP_NUM_THREADS=$(lscpu -p | egrep -v '^#' | sort -u -t, -k 2,4 | wc -l)

frameworks=("dace_cpu")


benchmarks=("azimint_hist" "azimint_naive"
            "cavity_flow" "channel_flow"
            "compute"
            "go_fast"
            "mandelbrot1" "mandelbrot2")
deep_learning=("conv2d_bias" "softmax" "mlp" "lenet" "resnet")
weather=("hdiff" "vadv")
polybench=("adi" "atax" "bicg" "cholesky" "correlation" "covariance" "deriche"
           "doitgen" "durbin" "fdtd_2d" "floyd_warshall" "gemm" "gemver"
           "gesummv" "gramschmidt" "heat_3d" "jacobi_1d" "jacobi_2d" "k2mm"
           "k3mm" "lu" "ludcmp" "mvt" "nussinov" "seidel_2d" "symm" "syr2k"
           "syrk" "trisolv" "trmm")


for i in "${benchmarks[@]}"
do
    for j in "${frameworks[@]}"
    do
	    echo Running $i with $j
		timeout 1000s python npbench/benchmarks/$i/$i.py -f $j
		fi
    done
done

for i in "${deep_learning[@]}"
do
    for j in "${frameworks[@]}"
    do
	    echo Running $i with $j
	    timeout 1000s python npbench/benchmarks/deep_learning/$i/$i.py -f $j
    done
done

for i in "${weather[@]}"
do
    for j in "${frameworks[@]}"
    do
	    echo Running $i with $j
	    timeout 1000s python npbench/benchmarks/weather_stencils/$i/$i.py -f $j
    done
done

for i in "${polybench[@]}"
do
    for j in "${frameworks[@]}"
    do
	    echo Running $i with $j
	    timeout 1000s python npbench/benchmarks/polybench/$i/$i.py -f $j
    done
done
