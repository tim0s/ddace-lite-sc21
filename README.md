# SC 21 Artifact evaluation

This repository contains the programs and scripts needed to reproduce
the results reported in the paper.


There are three main categories of results, that require different
hardware to be reproduced:
- single node results: programs run on  CPU and GPU (rthe results shown in Figure 6 and 7)
- FPGA results: programs run on FPGA (Intel and Xilinx -- results shown in Figure 8)
- distributed memory results: programs run on a cluster (results shown in Figure 11 and 12)

In the following, we describe the step necessary to reproduce each of them.


### Common prerequisites
 - Python 3.6-3.8 (the results were obtained with Python 3.8) 
 - Python virtual environment (venv) installation 
 - A C++14-capable compiler (tested with GCC 10.2.0)
 - NumPy version 1.19.2 with Intel MKL support



After cloning this repository, make sure you initialize submodules:

```
git submodule update --init
```

For plotting data, please install the indicated requirements:

```
pip install -r requirements.txt
```

The directory `machine-envs` contains the output from commands that gather execution environment information for
the different testbeds used in the paper.


### Python Environment

To run any of the experiments, you need to set up a suitable Python environment with the required modules. For each of the following sections, we provide a bash script that will create a new Python virtual environment with venv and install all the dependencies. The main format of these scripts is as follows:
- `git submodule update --init --recursive` to make sure all (sub-)sub-modules have been downloaded
- `python3 -m venv python3 venv` to create a new Python virtual environment
- `source venv/bin/activate` to activate the new Python virtual environment
- Installation of dependencies. For example, a common dependency is DaCe, which is provided as a (sub-)sub-module:
  - `python3 -m pip install --editable ./dace`

If for any reason you do not want to use venv, you can use other software, for example, Anaconda. Just create and activate a new Python virtual environment with your preferred software. In the case of Anaconda, you will have to execute something similar to:
```
conda create --name=myenv python=3.8
conda activate myenv
```

Just make sure to use Python 3.8 because the exact versions of the DaCe module provided here are not fully compatible with Python 3.9 or later. We use the `--editable` flag to ensure that DaCe is installed from the provided folder in each sub-module and not from PyPI. This is because the current latest version of DaCe on PyPI does not include the features needed to run the experiments.

#### Using an existing Python Environment

Since the DaCe module has numerous dependencies on other Python modules, we encourage you to create a new Python virtual environment. Nevertheless, if you want to utilize an existing Python environment, please note that DaCe has a minimum version requirement for some Python modules. Due to this, we suggest to install DaCe as follows:
```
python3 -m pip install --upgrade --upgrade-strategy eager --editable ./dace
```
The `--upgrade --upgrade-strategy eager` flags should ensure that any existing modules that do not satisfy DaCe's minimum requirements will be automatically upgraded. You can also check the `requirements.txt` for each of DaCe versions provided, for example, this [file](https://github.com/spcl/dace/blob/29c6beb29169c5a077b1731f940404425561ffc3/requirements.txt) for single-node experiments. Some modules that may cause issues if they are outdated are the following:
- cmake (3.15 or newer)
- networkx (2.5 or newer)
- sympy (1.7 or newer)


### Single Node CPU/GPU Results

This section describes the steps necessary to reproduce the results shown in Figure 6 and 7.

#### Additional Prerequisites
 - CuPy 8.3.0
 - Cuda Version 11.1
 - A Cuda Capable GPU (tested with v100)

#### Run the experiments


Befor running the experiments, please move into the `benchmark_shmem` folder and setup the Python virtual environment, by
running:

```
source setup_virtualenv.sh
```


This will create a virtual environment to
execute all the experiments.


To collect the data for Numpy, Dace, Numba, Pythran, Dace_GPU, CuPy, please run :
```Bash
./benchmark_shmem.sh
```

Please note that our setup scripts do not install Numba, Pythran and CuPy. It should be possible to install them through PyPI, i.e, `pip install numba pythran cupy`. However, (for performance reproducibility) there may be faster versions for your environment. For example, there is an optimized Numba version available through conda for an Intel-based machine : `conda install -c numba icc_rt`.

If needed, the user can edit the
script to perform tests only on certain frameworks and/or applications. You may find it helpful to use instead the two scripts in the `ddace-lite-sc21/single_node_scripts` folder (`sc21_cpu.sh` and `sc21_gpu.sh`), which run the CPU and GPU benchmarks separately. For CPU testing, please adapt the script to use the frameworks `numpy`, `numba`, `pythran`, and `dace_cpu`. For GPU testing, use the frameworks `numpy`, `cupy`, and `dace_gpu`. If you run the CPU tests with `numpy`, you do not need to the same for the GPU tests (they are a subset).

*Performance note*: `dace_cpu` uses OpenMP and best performance is usually achieved when `OMP_NUM_THREADS` is set to the number of physical cores (at least on Intel processors with HyperThreading). The scripts try to set the variable automatically but you may want to confirm the actual value or even set it manually.


All the execution times are collected in corresponding `csv` files, one per benchmark/framework,
that are stored in the directory where benchmarks scripts have been executed.


To run experiments and collect the data for GCC/ICC, please move on the `polybench_c` folder located in the root directory of this repository and run:
```
./benchmark_polybench run-icc /path/to/polybench --parallel --repetitions=10 --flags="-DEXTRALARGE_DATASET"
```

where `/path/to/polybench`, is the path to the folder containing the C version of polybench benchmarks (can be downloaded from
https://web.cse.ohio-state.edu/~pouchet.2/software/polybench/).

The execution times are collected in a `json` file. This can be converted to a `csv` using the same script:

```
./benchmark_polybench json-to-csv <file.json>
```


#### Plot results

To reproduce the plot shown in Figure 6, please move to the `comptimes` folder located in the root directory of this repository and run:
```
python comptimes.py
```

To reproduce the plot shown in Figure 7, please move to the `cpu_results` folder located in the root directory of this
repository and run:

```
python heatmap.py -f <path_to_folder_containing_csv_files> -p <path_to_folder_containing_polybench_results>
```

where the two `path_to_folder` arguments should point to the folder containing data produced at the previous step.

This will produce a file named `heatmap.pdf`.
For your convenience, we also included in this repository all the data used to produce Figure 7.
This can be done by just invoking the script without additional parameters:

```
python heatmap.py
```


To reproduce the plot shown in Figure 8, please move to the `gpu_results` folder located in the root directory of this
repository and run:

```
python gpu_plot.py -f <path_to_folder_containing_csv_files> 
```

that will produce a file named `gpuplot.pdf`.
Also in this case we included all the data necessary to produce the figure shown in the paper: to have this, please invoke
the above script without arguments.


### FPGA Results

This section will allow the user to reproduce the results shown in Figure 9.


#### Additional Prerequisites 

- Xilinx Vitis 2020.2 (this version of the artifact is incompatible with Vitis 2021.x or newer)
- A PCIe-attached Xilinx board. Tested with Xilinx Alveo U250 with `xilinx_u250_xdma_201830_2` shell
- Intel OpenCL SDK for FPGA (tested with version 20.3)
- A PCIe-attached Intel FPGA board. Tested with BittWare 520N (Stratix 10) FPGA board with the
`p520_max_sg280l` BSP installed.
  
#### Run the experiments

To execute the experiments, please move into the `benchmark_fpga` folder and setup the Python virtual environment, by
running:

```
source setup_virtualenv.sh
```

Synthesing hardware for FPGA could require several hours for each benchmark. 
To synthesize the hardware for a given polybench kernel:

```
./polybench_fpga.py build <platform> <kernel>
```

where `platform` could be `intel_fpga` or `xilinx`.

Once the build completed, the benchmark can be run in hardware:

```
./polybench_fpga.py run <platform> <kernel>
```

After the execution of each benchmark, a `csv` file will be produced in a folder with the benchmark name.
The file contains the execution times of the multiple executed runs.

The median, can be derived by using the `compute_median.py` script provided under `fpga_results/scripts`.


#### Plot results


To reproduce the plot shown in Figure 8, please move to the `fpga_results/plotting` folder located in the root directory of this
repository and run:

```
python plot.py -i <path_to_intel_fpga_csv> -x <path_to_xilinx_csv>
```

where the two `path_to_` arguments should point to the `csv` files containing the result for Intel FPGA and Xilinx,
one benchmark per row.

For your convenience, we included in this repository the file used to produce Figure 9.
This can be done by launching the script without any additional arguments.


### Distributed Memory Results
This allows to reproduce the results shown in Figure 12.

#### Additional Prerequisites
 
 - MPI compatible installation (tested with Cray MPI)
 - a slurm based queuing system
 - Dask, version 2.31. Can be installed through conda-forge
 - mpi4py python module: it can be installed using `pip` (requires MPI)
 - pytest module: it can be installed using `pip` (`python -m pip install pytest`)
 - Intel MKL/Scalapack


#### Environment issues

To simplify linking with the Intel MKL/Scalapack libraries, the `$MKLROOT$` env variable must be set. If Intel MKL is provided through a module in your system, then it is likely that the variable is set automatically when loading the module. If you are using Anaconda or Miniconda, you can install the libraries with:
```bash
conda install mkl mkl-include
export MKLROOT=$CONDA_ROOT # Substitute $CONDA_ROOT with the appropriate path, e.g., $HOME/anaconda3 or $HOME/miniconda3/envs/myenv
```
If you are unsure about where Intel MKL is installed, check the `include` and `lib` folders under `$CONDA_ROOT` or `$CONDA_ROOT/envs/myenv`. You are looking for the `mkl.h` header file and the library files starting with `libmkl`.


The distributed DaCe benchmark scripts use mpi4py. Python scripts using mpi4py are executed as any other application utlizing MPI, i.e., `mpirun/mpiexec/srun <options> python <python-script>`. However, mpi4py must be compiled with the same MPI library as the one corresponding to the MPI runner used to execute the Python script. Normally, you install mpi4py with `python -m pip install mpi4py`. If there are concerns about which MPI library is mpi4py linking against, it is possible to set the location of `mpicc` (for more details see mpi4py [documentation](https://mpi4py.readthedocs.io/en/stable/install.html)):
```bash
env MPICC=/path/to/mpicc python -m pip install mpi4py
```

To run the distributed DaCe tests, CMake must link properly to Intel MKL Scalapack and MPI. The first step is to get the correct link line using the Intel MKL [link line advisor](https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/onemkl/link-line-advisor.html). We have tested DaCe with MPICH and Cray MPI. The link line for these MPI libraries (also compatible with IntelMPI) is:
```
-L $MKLROOT/lib -lmkl_scalapack_lp64 -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -lgomp -lpthread -lm -ldl
```
In our systems, we found that it is also necessary to explicitly link to the MPI libraries, for example:
```
-L $MKLROOT/lib -lmkl_scalapack_lp64 -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 ${MPI_CXX_LINK_FLAGS} -lgomp -lpthread -lm -ldl
```
Furthermore, we found that, in Cray systems, CMake may have difficulty finding the MPI library. This issue can be solved by substituting `${MPI_CXX_LINK_FLAGS}` with the explicit path to the library. For example, this could be similar to `-L /usr/lib64/mpich/lib -lmpich`. The complete link line becomes:
```
-L $MKLROOT/lib -lmkl_scalapack_lp64 -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -L /usr/lib64/mpich/lib -lmpich -lgomp -lpthread -lm -ldl
```
Please copy this line to the `cmake_link_flags` field in `dace/libraries/pblas/environments/intel_mkl.py`, in the `benchmark_distmem/dace` folder. Furthermore, you can find the exact CMake configuration that we used for Piz Daint in `ddace-lite-sc21/distributed/daint-scripts/intel_mkl.py`


#### Run the experiments

Befor running the experiments, please move into the `benchmark_distmem` folder and setup the Python virtual environment, by
running:

```
source setup_virtualenv.sh
```

To reproduce the results in Figure 11 (comparison against Dask), first install `dask`, `dask-image`, and `dask-mpi`. If you are using `conda`, then you can install them with:
```Bash
conda install -c conda-forge dask dask-image dask-mpi
```

Then, please execute:
```Bash
srun -N P dask/polybenchdask.py
```

This will run the experiments by using P-2 workers. For more information on setting the number of processes, please read the short [documentation](https://spclgitlab.ethz.ch/alexnick/dace-python-frontend-samples/-/blob/115ddc7a7795c753bbb1376c29ea6a9279a2e9a9/dask/README.md). You should also set the correct number of cores in the [polybenchdask.py](https://spclgitlab.ethz.ch/alexnick/dace-python-frontend-samples/-/blob/115ddc7a7795c753bbb1376c29ea6a9279a2e9a9/dask/polybench_dask.py) script, line 6. 

To reproduce the results in Figure 12, please execute:

```
mpirun -n P python sc21_bench.py
```
This will run the experiments using P workers. You may find in the `distributed/daint-scripts` folder some example batch-scripts for running DaCe on Piz Daint. We also included a DaCe PBLAS library environment file for properly linking against Cray-MPICH on Piz Daint. If the benchmark `doitgen` exhibits very low performance (should take roughly as long as `atax`), then try executing the script with the following variable that instructs DaCe to use MKL:
```
DACE_library_blas_default_implementation=MKL mpirun -n P python sc21_bench.py
```

To reproduce the Legate results you must install `legate.core` and `legate.numpy` according to the developers instructions for your machine (please see https://github.com/nv-legate). Then, you can execute individual experiments with the following syntax:
```
USE_CUDA=0 LEGATE_DIR=/path/to/legate/build /path/to/quickstart/scripts/run.sh <N> sc21_legate.py -s <2*N> -b <benchmark-name> -lg:numpy:test --eager-alloc-percentage 10
```
In the above, N is the number of nodes, while benchmark-name is one of {atax, bicg, doitgen, gemm, gemver, gesymmv, k2mm, k3mm, mvt, jacobi_1d, jacobi_2d}. We have included in the `distributed/daint-scripts` folder a custom `run-mc.sh` script (provided by the Legate developers) for running Legate in the multi-core nodes of Piz Daint.

#### Plot results


To reproduce the plot shown in Figure 12, please move to the `distributed` folder located in the root directory of this
repository and run:

```
python plot.py -f <path_to_folder_csv_files> 
```


where the two `path_to_folder` argument should point to the folder containing data produced at the previous step.

This will produce a file named `facet.pdf`.
For your convenience, we also included in this repository all the data used to produce Figure 11:
this can be done by just invoking the script without additional parameters


<!-- To reproduce the plot shown in Figure 12, please move to the `legate_results` folder located in the root directory of this
repository and run:

```
python plot_script.py
```

This will produce a file named `legateplot.pdf`. -->
For your convenience, we also included in this repository all the data used to produce Figure 12:
this can be done by just invoking the script without additional parameters

