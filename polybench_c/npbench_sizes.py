import click
import csv

SIZES = {
    "adi": "MEDIUM",
    "gramschmidt": "MEDIUM",
    "nussinov": "MEDIUM",
    "seidel": "MEDIUM",
    "seidel_2d": "MEDIUM",
    "cholesky": "LARGE",
    "correlation": "LARGE",
    "covariance": "LARGE",
    "fdtd-2d": "LARGE",
    "fdtd_2d": "LARGE",
    "floyd-warshall": "LARGE",
    "floyd_warshall": "LARGE",
    "heat": "LARGE",
    "heat-3d": "LARGE",
    "heat_3d": "LARGE",
    "lu": "LARGE",
    "ludcmp": "LARGE",
    "symm": "LARGE",
    "syr2k": "LARGE",
    "syrk": "LARGE",
    "trmm": "LARGE",
    "deriche": "EXTRALARGE",
    "doitgen": "EXTRALARGE",
    "gemm": "EXTRALARGE",
    "jacobi-2d": "EXTRALARGE",
    "jacobi_2d": "EXTRALARGE",
    "atax": "EXTRAEXTRALARGE",
    "bicg": "EXTRAEXTRALARGE",
    "durbin": "EXTRAEXTRALARGE",
    "gemver": "EXTRAEXTRALARGE",
    "gesummv": "EXTRAEXTRALARGE",
    "jacobi-1d": "EXTRAEXTRALARGE",
    "jacobi_1d": "EXTRAEXTRALARGE",
    "2mm": "EXTRAEXTRALARGE",
    "3mm": "EXTRAEXTRALARGE",
    "k2mm": "EXTRAEXTRALARGE",
    "k3mm": "EXTRAEXTRALARGE",
    "mvt": "EXTRAEXTRALARGE",
    "trisolv": "EXTRAEXTRALARGE"
}


@click.command()
@click.argument("input_csv" , type=click.Path(exists=True))
@click.argument("output_csv", type=click.Path())
def npbench_sizes(input_csv, output_csv):
    """
    Filters the CSV-file produced by the benchmarking script to use the sizes
    evaluated in NPBench.
    """
    with open(input_csv, "r") as in_file:
        with open(output_csv, "w") as out_file:
            writer = csv.writer(out_file)
            for row in csv.reader(in_file, delimiter=","):
                for col in row:
                    if col in SIZES:
                        if SIZES[col] in row:
                            writer.writerow(row)

if __name__ == "__main__":
        npbench_sizes()
