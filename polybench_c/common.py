import datetime
import click
import pathlib


SCRIPT_DIR = pathlib.Path(__file__).absolute().parent

class Colors:
    SUCCESS = "\033[92m"
    STATUS = "\033[94m"
    WARNING = "\033[93m"
    ERROR = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    END = "\033[0m"


def print_status(message):
    timestamp = datetime.datetime.now().strftime("%H:%M:%S")
    click.echo(f"{Colors.STATUS}{Colors.BOLD}[{timestamp}]{Colors.END} {message}")


def print_success(message):
    timestamp = datetime.datetime.now().strftime("%H:%M:%S")
    click.echo(f"{Colors.SUCCESS}{Colors.BOLD}[{timestamp}]{Colors.END} {message}")


def print_error(message):
    timestamp = datetime.datetime.now().strftime("%H:%M:%S")
    click.echo(f"{Colors.ERROR}{Colors.BOLD}[{timestamp}]{Colors.END} {message}")
