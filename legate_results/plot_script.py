import copy
import glob
import math
import seaborn as sns
import pandas as pd
from pathlib import Path
import numpy as np
import matplotlib
import math
# matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.patches import Rectangle

sns.set(rc={'figure.figsize':(8,4)})
sns.set_style("whitegrid")

# Read data
data = None
pathlist = glob.glob('*.csv') 
for f in pathlist:
	ndata = pd.read_csv(f)
	if data is None:
		data = ndata
	else:
		data = pd.concat([data, ndata])


# failures = pd.read_csv('NumPy Benchmarks - Failures.csv')

#get rid of kind and dwarf, we don't use them
data = data.drop(['sockets', 'sizes'], axis=1).reset_index(drop=True)

data = data.replace(
{'jacobi_1d':'jacobi1d',
'jacobi_2d':'jacobi2d'}
)

# for each framework and benchmark, choose only the best details,mode (based on median runtime), then get rid of those
aggdata = data.groupby(["benchmark", "framework"], dropna=False).agg({"time": np.median}).reset_index()
ptable = aggdata.pivot_table(index=["benchmark"], columns="framework", values="time").reset_index()
ptable["speedup"] = ptable["legate_cpu"] / ptable["dace_cpu"]
data = data.sort_values('benchmark')
ax = sns.barplot(y="time", x="benchmark", hue="framework", data=data, palette=["#F55B4E", "#69A8F5"])
ax.legend_.remove()
ax.text(1.7, .13, "Out Of Memory", rotation=90)
plt.xticks(rotation=90)
ax.set(ylabel="Runtime (ms)", xlabel="Benchmark", yscale="log")

for p in range(len(ptable)):
	t = ptable.speedup[p]
	t = str(round(t,1))
	y = ptable.dace_cpu[p]
	if t != "nan":
		plt.text(x=p, y=y+0.1*y, s=t, horizontalalignment='left', size='9', color='black', weight='semibold')


legend_elements = [
	Patch(facecolor='#F55B4E', edgecolor='black', label='Legate'),
	Patch(facecolor='#69A8F5', edgecolor='black', label='DaCe'),
	Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0, label="Speedup (annot.)")

]


ax.legend(handles=legend_elements, loc='upper left')

plt.tight_layout()
plt.savefig("legateplot.pdf", dpi=600)
plt.show()
